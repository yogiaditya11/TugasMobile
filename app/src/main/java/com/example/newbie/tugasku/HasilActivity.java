package com.example.newbie.tugasku;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HasilActivity extends AppCompatActivity {

    public static String PK = "data";
    private String terimaData = null;
    private TextView txtData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        txtData = findViewById(R.id.txt_terima);
        terimaData = getIntent().getStringExtra(PK);
        txtData.setText(terimaData);
    }
}
