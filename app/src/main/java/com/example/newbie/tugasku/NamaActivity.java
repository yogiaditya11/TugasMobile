package com.example.newbie.tugasku;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NamaActivity extends AppCompatActivity {

    private Button btn1;
    private String strhasil;
    private EditText txtEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nama);

        btn1 = findViewById(R.id.btn_nama);

        txtEdit = findViewById(R.id.txt_nama);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strhasil = txtEdit.getText().toString();
                Intent intent = new Intent(NamaActivity.this, HasilActivity.class);
                intent.putExtra(HasilActivity.PK, strhasil);
                startActivityForResult(intent, 0);
            }
        });


    }
}
